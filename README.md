# README #

Não é necessário fazer nenhuma instalação em sua máquina local para avaliar o sistema, o sistema está hosteado no Heroku, e há um vídeo demonstrativo no Youtube.

Vídeo: https://youtu.be/WsCApUHHW0A
Aplicação: https://pure-bastion-24489.herokuapp.com/

### Tecnologias Utilizadas ###

* Linguagens de Programação/Marcação: Java, HTML, CSS3, JavaScript.
* Frameworks: Hibernate, Spring, Bean.
* Banco de Dados: PostgreSQL / MySQL.
* Outras tecnologias: Maven.

### Como instalar o sistema? ###

* Não é necessário instalar o sistema, visto que ele está hosteado no Heroku.
* Basta acessar o link: https://pure-bastion-24489.herokuapp.com/

### Dúvidas? ###

* thiagocdeabreu@hotmail.com